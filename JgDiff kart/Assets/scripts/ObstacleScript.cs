using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{
    private GameObject player;
    private GameObject playerObject;
    private Rigidbody rb;
    Vector3 pushDirection;
    float knockbackDistance = 15;
    float baseKnockBackDistance = 15;
    float boostedKnockbackDistance;
    int knockbackduration = 1;
    int baseKnockbackduration = 1;
    int boostedKnockbaclDuration;
    Collider collider;


    private playerMovement playerMov;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Kurkkumopo");
        rb = player.GetComponent<Rigidbody>();
        playerObject = GameObject.Find("Kurkkumopo/knockback");
        playerMov = GameObject.Find("Kurkkumopo").GetComponent<playerMovement>();
        boostedKnockbackDistance = knockbackDistance * 1.5f;
        boostedKnockbaclDuration = 2 * baseKnockbackduration;
        collider = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
         
        pushDirection = playerObject.transform.position - transform.position ;
        if(playerMov.speedActive)
        {
            knockbackDistance = boostedKnockbackDistance;
            knockbackduration = boostedKnockbaclDuration;
        }
        else if (playerMov.speedActive)
        {
            knockbackDistance = baseKnockBackDistance;
            knockbackduration = baseKnockbackduration;
        }

        if(playerMov.isInvinsible)
        {
            collider.enabled = false;
        }
        else if(!playerMov.isInvinsible)
        {
            collider.enabled = true;
        } 
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            playerMov.knockbackActive = true;
            rb.AddForce(pushDirection * knockbackDistance, ForceMode.Impulse);
            StartCoroutine(StunDuration());
            
        }
    }
    IEnumerator StunDuration()
    {
        yield return new WaitForSeconds(knockbackduration);
        playerMov.knockbackActive = false;
    }
}
