using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float time = 3;
    public TextMeshProUGUI abilityCd;
    public GameObject abilityText;
    public bool timerActive;
    public TextMeshProUGUI globalTimer;
    public float globalTime = 0;
    public bool over;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        GlobalTimer();
        if (time < 0)
        {
            abilityText.SetActive(false);
            time = 3;
            timerActive = false;
        }
        if(timerActive)
        {
            time -= Time.deltaTime;
            abilityCd.text = "Time Left: " + time.ToString("F2");
        }
        
    }
    public void AbilityTimer()
    {
        abilityText.SetActive(true);
        timerActive = true;
    }
    public void GlobalTimer()
    { if(!over)
        globalTime += Time.deltaTime;
        globalTimer.text = "Your Time is: " + globalTime.ToString("F2");
    }
}
