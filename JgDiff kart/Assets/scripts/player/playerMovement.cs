using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class playerMovement : MonoBehaviour
{
    public float speed;
    float baseSpeed = 25;
    public float rotateSpeed;
    public Transform player;
    Rigidbody rb;

    private Timer timer;

    //public gameOver gameover;

    float verticalInput;
    public float horizontalInput;

    public bool gameOver;

    public bool grounded;
      
    int buffId;
    public bool isBuffActive = false;
    float buffedSpeed = 50;
    float buffJumpHeight = 10;
    float airSpeed = 30;
    float buffDuration = 3;
    public bool isInvinsible = false;
    public bool speedActive = false;

    public GameObject speedIcon;
    public GameObject jumpIcon;
    public GameObject invisIcon;

    public bool knockbackActive = false;

    public float baseFov;

    public gameOver manager;
  
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        baseFov = Camera.main.fieldOfView;
        //manager = GameObject.Find("gameManager").GetComponent<gameOver>();
        timer = GameObject.Find("gameManager").GetComponent<Timer>();
        //gameover = GameObject.FindGameObjectWithTag("background").GetComponent<gameOver>();
        Cursor.lockState = CursorLockMode.Locked;
        Physics.gravity *= 1;
    }

    // Update is called once per frame
    void Update()
    {
        SpeedControl();
        buff();
        Movement();
    }

    void Movement()
    {
        if (!manager.isGameOver && !knockbackActive)
        {
            verticalInput = Input.GetAxis("Vertical");
            horizontalInput = Input.GetAxis("Horizontal");
            Vector3 move = -1 * player.forward;
            Vector3 rotate = new Vector3(0, rotateSpeed * horizontalInput, 0);
            rb.AddForce(move.normalized * speed, ForceMode.Force);
            transform.Rotate(rotate * Time.deltaTime);

            if(!grounded && !speedActive)
            {
                speed = airSpeed;
            }
            else if (grounded && !speedActive)
            {
                speed = baseSpeed;
            }
        }
    }

    void SpeedControl()
    {
        Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        if (flatVel.magnitude > speed)
        {
            Vector3 limitedVel = flatVel.normalized * speed;
            rb.velocity = new Vector3(limitedVel.x, rb.velocity.y, limitedVel.z);
        }
    }

    void buff()
    {
        if (buffId ==  1 && Input.GetKeyDown(KeyCode.Space) && isBuffActive)
        {
            speed = buffedSpeed;
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, baseFov + 10, 0.5f);
            speedActive = true;
            timer.abilityText.SetActive(true);
            timer.AbilityTimer();
            StartCoroutine(BuffDuration());
        }
        else if(buffId == 2 && Input.GetKeyDown(KeyCode.Space) && isBuffActive)
        {
            rb.AddForce(Vector3.up * buffJumpHeight, ForceMode.Impulse);
            isBuffActive = false;
            buffId = Random.Range(1, 4);
            jumpIcon.SetActive(false);
        }
        else if (buffId == 3 && Input.GetKeyDown(KeyCode.Space) && isBuffActive)
        {
            timer.AbilityTimer();
            isInvinsible = true;
            StartCoroutine(BuffDuration());
        }
    }
    IEnumerator BuffDuration()
    {
        yield return new WaitForSeconds(buffDuration);
        speed = baseSpeed;
        Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, baseFov, 0.5f);
        isBuffActive = false;
        isInvinsible = false;
        speedActive = false;
        speedIcon.SetActive(false);
        invisIcon.SetActive(false);
        buffId = Random.Range(1, 4);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Grass" && !isInvinsible)
        {
            manager.Lose();
            rb.velocity = new Vector3(0, 0, 0);
            gameOver = true;
        }
        if (other.gameObject.tag == "Pickup" && !isBuffActive)
        {
            isBuffActive = true;
            buffId = Random.Range(1, 4);
            Destroy(other.gameObject);
            if(buffId == 1)
            {
                speedIcon.SetActive(true);
            }
            else if (buffId == 2)
            {
                jumpIcon.SetActive(true);
            }
            else if (buffId == 3)
            {
                invisIcon.SetActive(true);   
            }
        }
        if (other.gameObject.tag == "Ground")
        {
            
            grounded = true;
        }
        else
        {
            grounded = false;
        }
        // checkpoint checker
        
    }
}


