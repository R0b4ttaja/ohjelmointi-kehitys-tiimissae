using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackCheckpoints : MonoBehaviour
{
    public List<Transform> checkpointSinglesList = new List<Transform>();
    public GameObject checkpoints;
    public int indexValue = 0;
    public int winCounter = 0;

    private void Awake()
    {
        checkpoints = GameObject.Find("Checkpoints");
        foreach ( Transform checkpoint in checkpoints.transform)
        {
            CheckpointSingle checkpointSingle = checkpoint.GetComponent<CheckpointSingle>();
            checkpointSinglesList.Add(checkpoint);
            
        }
    }
    private void Update()
    {

    }
}
