using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinnishLine : MonoBehaviour
{
    private gameOver gameOver;
    public int indexValue = 0;
    public int winCounter = 0;
    CheckpointSingle checkpointSingle;
    // Start is called before the first frame update
    void Start()
    {
        gameOver = GameObject.Find("gameManager").GetComponent<gameOver>();
        checkpointSingle = GameObject.Find("Checkpoints/Checkpoint1").GetComponent<CheckpointSingle>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")&& checkpointSingle.canWin)
        {
            gameOver.Win();
        }
    }
}
