using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointSingle : MonoBehaviour
{

    private FinnishLine MaaliViiva;
    GameObject checkpoints2;
    public List<Transform> checkpointSinglesList2 = new List<Transform>();
    public bool canWin;
    
    private void Awake()
    {

        MaaliViiva = GameObject.Find("Maali/LineCheck").GetComponent<FinnishLine>();
        checkpoints2 = GameObject.Find("Checkpoints");
        foreach (Transform checkpoint2 in checkpoints2.transform)
        {
            checkpointSinglesList2.Add(checkpoint2);
        }
        //Debug.Log(checkpoints2.transform.childCount);
    }
    private void LateUpdate()
    {
        if (MaaliViiva.winCounter == checkpoints2.transform.childCount && !canWin)
        {
            canWin = true;
            
            
        }
    }
     
private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && checkpointSinglesList2[MaaliViiva.indexValue].name == gameObject.name)
        {
            MaaliViiva.indexValue++;
            MaaliViiva.winCounter++;
            Debug.Log(MaaliViiva.indexValue);
            if (MaaliViiva.indexValue ==  checkpointSinglesList2.Count)
            {
                MaaliViiva.indexValue --;
            }
        }
    }
}
