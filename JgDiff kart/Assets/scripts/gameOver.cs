using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class gameOver : MonoBehaviour
{
    public bool isGameOver = false;
    public TextMeshProUGUI timeText;
    public Timer timer;
    public GameObject panel;
    public GameObject background;
    public void Lose() 
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        background.SetActive(true);
        isGameOver = true;
    }
    public void restart()
    {
        isGameOver = false;
        SceneManager.LoadScene("base");
    }
    public void Win()
    {
        timeText.text = "Your Time " + timer.globalTime.ToString("F2");
        timer.over = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        panel.SetActive(true);
        isGameOver = true;
    }
}
