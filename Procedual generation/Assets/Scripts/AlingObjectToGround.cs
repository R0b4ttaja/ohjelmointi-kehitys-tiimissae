using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlingObjectToGround : MonoBehaviour
{
    private void Start()
    {
            Ray ray = new Ray(transform.position, -transform.up);
            if (Physics.Raycast(ray, out RaycastHit hit, 100))
            {
                if (hit.collider != null)
                {
                    transform.position = new Vector3(transform.position.x, hit.transform.position.y + hit.point.y, transform.position.z);
                
                }
            }
    }
}
