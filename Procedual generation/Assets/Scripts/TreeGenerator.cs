using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGenerator : MonoBehaviour
{
    [SerializeField] private GameObject[] trees;
    [SerializeField] private int treeCount;
    [SerializeField] private Transform spawnArea;
    private Vector3 currentPosition;
    private List<Vector3> positionsUsed = new List<Vector3>();

    private int spawningTryCap = 1000;


    private void Start()
    {
        SpawnTrees();
    }
    private void SpawnTrees() 
    {
        int findRightSpotTries;
        findRightSpotTries = 0;
        for (int i = 0; i < treeCount; i++)
        {
            
            currentPosition = GetSpawnArea();
            if(!IsTooCloseToAnotherObject(currentPosition))
            {
                findRightSpotTries = 0;
                Instantiate(trees[Random.Range(0, trees.Length)], currentPosition, Quaternion.Euler(0, Random.Range(0f, 360f), 0), transform);
                positionsUsed.Add(currentPosition);
            }
            else
            {
                i--;
                findRightSpotTries++;
                if(findRightSpotTries >= spawningTryCap) 
                {
                    break;
                }
            }
            
        }
        Debug.Log(transform.childCount);
    }

    private Vector3 GetSpawnArea()
    {
        Collider collider = spawnArea.GetComponent<Collider>();
        float areaX = Random.Range(spawnArea.position.x, spawnArea.position.x + collider.bounds.size.x);
        float areaZ = Random.Range(spawnArea.position.z, spawnArea.position.z + collider.bounds.size.z);
        return new Vector3(areaX, spawnArea.position.y + 20, areaZ);
    }
    private bool IsTooCloseToAnotherObject(Vector3 currentVector)
    {
        bool isTooClose = false;
        foreach (Vector3 vector in positionsUsed)
        {
           if(vector.x <= currentVector.x + 2.5f && vector.x >= currentVector.x - 2.5f) 
            {
                if (vector.z <= currentVector.z + 2.5f && vector.z >= currentVector.z - 2.5f)
                {
                    isTooClose = true;
                }
                    
            }
        }
        return isTooClose;
    }

}

