using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingCamera : MonoBehaviour
{
    [SerializeField] private float speed = 30;
    [SerializeField] private float sensitivity = 5;
    private float horizontalInput;
    private float verticalInput;
    private float Xrotation;
    private float Yrotation;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        WASDMovement();
        RotateCamera();
    }
    private void WASDMovement()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        Vector3 dir = transform.forward * verticalInput + transform.right * horizontalInput;

        transform.position += dir * speed * Time.deltaTime;
    }
    private void RotateCamera()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        Xrotation -= mouseY * sensitivity;
        Yrotation += mouseX * sensitivity;
        // caps Xrotation to -90 - 90
        Xrotation = Mathf.Clamp(Xrotation, -90, 90);
        // changes cameras rotation in x axis 
        transform.rotation = Quaternion.Euler(Xrotation, Yrotation, 0f);

    }
}
