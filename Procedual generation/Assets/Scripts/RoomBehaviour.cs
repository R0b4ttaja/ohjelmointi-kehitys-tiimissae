using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomBehaviour : MonoBehaviour
{
    [SerializeField]private GameObject[] walls; // 0 = up 1 = down = 2 right 3 = left
    [SerializeField]private GameObject[] doors; // 0 = up 1 = down = 2 right 3 = left
    public void UpdateRoom(bool[] status)
    {
        // based on bool array activates specific doorways
        for(int i = 0; i< status.Length; i++) 
        {
            doors[i].SetActive(status[i]);
            walls[i].SetActive(!status[i]);
        }
    }
}
