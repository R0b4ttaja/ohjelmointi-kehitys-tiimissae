using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGenerator : MonoBehaviour
{
    public Vector2 size;
    public int startPos = 0;
    public GameObject room;
    public Vector2 offset;

    List<Cell> board;
    public class Cell
    {
        public bool isVisited = false;
        public bool[] status = new bool[4];
    }
    private void Start()
    {
        MazeGenerator();
    }
    void GenerateDungeon()
    {
        for (int x = 0; x < size.x; x++) 
        {
            for (int z = 0; z < size.y; z++)
            {
                var newRoom = Instantiate(room, new Vector3(x*offset.x,0,-z * offset.y), Quaternion.identity,transform).GetComponent<RoomBehaviour>();
                newRoom.UpdateRoom(board[Mathf.FloorToInt(x + z * size.x)].status);
            }
        }
    }
    private void MazeGenerator()
    {
        board = new List<Cell>();

        for (int x = 0; x < size.x; x++)
        {
            for (int z = 0; x < size.y; z++)
            {
                board.Add(new Cell());
            }
        }
        int currentCell = startPos;
        Stack<int> path = new Stack<int>();
        int k = 0;

        while (k < 1000)
        {
            k++;

            board[currentCell].isVisited = true;

            List<int> neighbors = CheckNeighbors(currentCell);

            if (neighbors.Count == 0)
            {
                if (path.Count == 0)
                {
                    break;
                }

                else
                {
                    currentCell = path.Pop();
                }
            }
            else
            {
                path.Push(currentCell);
                int newCell = neighbors[Random.Range(0, neighbors.Count)];

                if (newCell > currentCell)
                {
                    if (newCell - 1 == currentCell)
                    {
                        board[currentCell].status[2] = true;
                        currentCell = newCell;
                        board[currentCell].status[3] = true;
                    }
                    else
                    {
                        board[currentCell].status[1] = true;
                        currentCell = newCell;
                        board[currentCell].status[0] = true;
                    }
                }
                else
                {
                    if (newCell + 1 == currentCell)
                    {
                        board[currentCell].status[3] = true;
                        currentCell = newCell;
                        board[currentCell].status[2] = true;
                    }
                    else
                    {
                        board[currentCell].status[0] = true;
                        currentCell = newCell;
                        board[currentCell].status[1] = true;
                    }
                }
            }
        }
        GenerateDungeon();
    }

     List<int> CheckNeighbors(int cell)
    {
        List<int> neighbors = new List<int>();
        //check up neighbour
        if (cell - size.x >= 0 && !board[Mathf.FloorToInt(cell-size.x)].isVisited) 
        { 
        neighbors.Add(Mathf.FloorToInt(cell - size.x));
        }
        //check down neighbour
        if (cell + size.x < board.Count && !board[Mathf.FloorToInt(cell + size.x)].isVisited)
        {
            neighbors.Add(Mathf.FloorToInt(cell + size.x));
        }
        //check right neighbour
        if ((cell + 1) % size.x != 0 && !board[Mathf.FloorToInt(cell + 1)].isVisited)
        {
            neighbors.Add(Mathf.FloorToInt(cell + 1));
        }
        //check left neighbour

        if (cell % size.x != 0 && !board[Mathf.FloorToInt(cell - 1)].isVisited)
        {
            neighbors.Add(Mathf.FloorToInt(cell -1));
        }
        return neighbors;
    }
}
