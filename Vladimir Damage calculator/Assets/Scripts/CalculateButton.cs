using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalculateButton : MonoBehaviour
{
    private Button calculatebutton;
    [SerializeField] private CalculateVladDamage calculateVladDamage;
    // Start is called before the first frame update
    void Start()
    {
        calculatebutton = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            calculateVladDamage.SendTotalDamage();
        }
    }
}
