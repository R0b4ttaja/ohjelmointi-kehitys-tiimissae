using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static CalculateVladDamage;


public class CalculateVladDamage : MonoBehaviour
{
    public event EventHandler<TotalDamage> OnDamageSent;

    public class TotalDamage : EventArgs
    {
        public float totalDamage;
    }

    [SerializeField] private CountInputStat countInputStat;
    [SerializeField] private SendToggleData sendToggleData;
    private int[] qBaseDamage = {80,100,120,140,160};
    private int[] empQBaseDamage = { 148,185,222,259,296};
    private int[] wBaseDamage = {80,136,192,248,300};
    private int[] eBaseDamage = { 60, 90,120,150,180 };
    private int[] rBaseDamage = { 150, 250, 350 };

    private float ultDamageMultiplyer = 1.1f;


    private int baseHealth = 607;
    private int healthGrowth = 110;
    private int bonusHp;
    
    private float qApScaling = 0.6f;
    private float empQApScaling = 1.11f;
    private float wHealthScaling = 0.025f;
    private float eApScaling = 0.8f;
    private float eHeathScaling = 0.06f;
    private float rApScaling = 0.7f;

    private int qIndex;
    private int wIndex;
    private int eIndex;
    private int rIndex;

    




    private void Start()
    {

    }
    private void Update()
    {

        if(Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log(countInputStat.GetDamageReduction());
        }
    }
    private int GetBonusHp()
    {
        bonusHp = countInputStat.GetHealth() - (baseHealth + (countInputStat.GetLevel() -1) * healthGrowth);

        bonusHp = Mathf.Clamp(bonusHp,0, 100000000);
        return bonusHp;
    }

    private int SortQAblityPoints()
    {
        qIndex = 0;
        for (int i = 0; i < countInputStat.GetLevel(); i++)
        {
            if(i > 2 && i < 5 )
            {
                qIndex++;
            }
            if(i == 6 || i == 8)
            {
                qIndex++;
            }
        }
        return qIndex;
    }
    private int SortWAblityPoints()
    {
        wIndex = 0;
        for (int i = 0; i < countInputStat.GetLevel(); i++)
        {
            if(i > 12 && i <15 || i > 15) 
            {
                wIndex++;
            }
        }
        return wIndex;
    }

    private int SortEAblityPoints()
    {
        eIndex = 0;
        for (int i = 0; i < countInputStat.GetLevel(); i++)
        {
            if (i == 6 || i == 9 || i > 10 && i < 13)
            {
                eIndex++;
            }
        }
        return eIndex;
    }
    private int SortRAblityPoints()
    {
        rIndex = 0;
        for (int i = 0; i < countInputStat.GetLevel(); i++)
        {
            if (i == 10 || i == 15 )
            {
                rIndex++;
            }
        }
        return rIndex;
    }
    private float QDamage()
    {
        if(sendToggleData.GetEmpoweredQData())
        {
            return empQBaseDamage[SortQAblityPoints()] + countInputStat.GetAbilityPower() * empQApScaling;
        }
        else
        {
            return qBaseDamage[SortQAblityPoints()] + countInputStat.GetAbilityPower() * qApScaling;
        }
         
         
    }
    private float WDamage()
    {
        return wBaseDamage[SortWAblityPoints()] + GetBonusHp() * wHealthScaling;

    }
    private float EDamage()
    {
        return eBaseDamage[SortEAblityPoints()] + countInputStat.GetAbilityPower() * eApScaling + GetBonusHp() * eHeathScaling;

    }

    private float RDamage()
    {
        return rBaseDamage[SortRAblityPoints()] + countInputStat.GetAbilityPower() * rApScaling;

    }

    private float CalculateTotalDamage()
    {
        if (sendToggleData.GetUltToggleData())
        {
            return countInputStat.GetDamageReduction() * (RDamage() + (ultDamageMultiplyer * EDamage() + QDamage() + WDamage()));
        }
        else
        {
            return countInputStat.GetDamageReduction() * (EDamage() + QDamage() + WDamage());
        }
        
    }
   
    public void SendTotalDamage()
    {
        OnDamageSent?.Invoke(this, new TotalDamage { totalDamage = CalculateTotalDamage()});
    }

}
