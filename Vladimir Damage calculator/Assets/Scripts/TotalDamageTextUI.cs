using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class TotalDamageTextUI : MonoBehaviour
{
    [SerializeField] private CalculateVladDamage calculateVladDamage;
    [SerializeField] private TextMeshProUGUI totalDamageText;
    private void Start()
    {
        calculateVladDamage.OnDamageSent += CalculateVladDamage_OnDamageSent;
    }

    private void CalculateVladDamage_OnDamageSent(object sender, CalculateVladDamage.TotalDamage e)
    {
        totalDamageText.text = e.totalDamage.ToString();
    }
}
