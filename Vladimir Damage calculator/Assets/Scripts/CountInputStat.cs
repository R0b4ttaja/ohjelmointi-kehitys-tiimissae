using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CountInputStat : MonoBehaviour
{
    [SerializeField]private TMP_InputField inputfieldAp;
    [SerializeField] private TMP_InputField inputfieldLevel;
    [SerializeField] private TMP_InputField inputfieldHealth;
    [SerializeField] private TMP_InputField inputfieldMagicResist;
    int apInt;
    int levelInt;
    int HealthInt;
    float magicResist = 0;

   
    public int GetAbilityPower()
    {
        if (inputfieldAp.text.Length > 0)
        {
            apInt = int.Parse(inputfieldAp.text);

            return apInt;
        }
        return 0;
        
    }
    public int GetLevel()
    {
        if (inputfieldLevel.text.Length > 0)
        {
            levelInt = int.Parse(inputfieldLevel.text);
            levelInt = Mathf.Clamp(levelInt, 1, 18);

            return levelInt;
        }
        return 0;
    }
    public int GetHealth()
    {
        if (inputfieldHealth.text.Length > 0)
        {
            HealthInt = int.Parse(inputfieldHealth.text);
            return HealthInt;
        }
        return 0;
    }
    public float GetDamageReduction()
    {
        if(inputfieldMagicResist.text.Length <= 0)
        {
            return 1f;
        }
        magicResist = float.Parse(inputfieldMagicResist.text);
        return 100 / (100 + magicResist);
    }

}
