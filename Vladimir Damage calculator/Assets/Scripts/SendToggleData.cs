using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendToggleData : MonoBehaviour
{
    [SerializeField] private Toggle isUltiActiveToggle;
    [SerializeField] private Toggle isQEmpoweredToggle;

    private bool isUltiActive;
    private bool isQEmpowered;

    private void Update()
    {
        isUltiActive = isUltiActiveToggle.isOn;
        isQEmpowered = isQEmpoweredToggle.isOn;
    }
    public bool GetUltToggleData()
    {
        return isUltiActive;
    }
    public bool GetEmpoweredQData()
    {
        return isQEmpowered;
    }

    
}
